import Button from './Button'
import { useState } from 'react'

const AddItem = ({ styles, onNewItem }) => {
    const [state, setState] = useState({
        title: '',
        price: 0,
        quantity: 0,
    })

    const resetForm = () => {
        setState({ title: '', price: 0, quantity: 0 })
    }

    const onSubmit = (event) => {
        event.preventDefault()

        if (!state.title) {
            alert('Please enter a value for title')
            return
        }

        onNewItem({
            title: state.title,
            price: state.price,
            quantity: state.quantity,
        })
        resetForm()
    }

    return (
        <form className={styles.form.container}>
            <div>
                <label className={styles.form.label}>Title</label>
                <input
                    type="text"
                    className={styles.form.input}
                    placeholder="Item Title"
                    value={state.title}
                    onChange={({ target }) => {
                        setState({ ...state, title: target.value })
                    }}
                />
            </div>
            <div>
                <label className={styles.form.label}>Price</label>
                <input
                    type="number"
                    className={styles.form.input}
                    placeholder="Item Price"
                    value={state.price}
                    onChange={({ target }) => {
                        setState({ ...state, price: Number(target.value) })
                    }}
                />
            </div>
            <div>
                <label className={styles.form.label}>Quantity</label>
                <input
                    type="number"
                    className={styles.form.input}
                    placeholder="Item Quantity"
                    value={state.quantity}
                    onChange={({ target }) => {
                        setState({ ...state, quantity: Number(target.value) })
                    }}
                />
            </div>

            <Button
                className={styles.button.block}
                styles={styles.margin.top}
                event={onSubmit}
                title="Submit"
            />
        </form>
    )
}

export default AddItem
