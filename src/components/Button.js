const Button = ({ className, styles, event, title }) => {
    return (
        <>
            <button className={className} style={styles} onClick={event}>
                {title}
            </button>
        </>
    )
}

Button.defaultProps = {
    className: 'w3-btn w3-green w3-margin w3-ripple',
    title: 'Submit',
}

export default Button
