import Button from './Button'

const Item = ({ item, styles, onDeleteItem, onAddItem, onCutItem }) => {
    return (
        <div className={styles.item.w3} style={styles.item.flex} key={item.id}>
            <div className="details">
                <h2>{item.title}</h2>
                <h5>Quantity: {item.quantity}</h5>
                <h5>Price: ${item.price}</h5>
                <h5>Cost: ${item.price * item.quantity}</h5>
            </div>
            <div className="buttons">
                <Button
                    className={
                        item.quantity === 0
                            ? styles.button.disable
                            : styles.button.cut
                    }
                    styles={styles.button.margin}
                    event={() => onCutItem(item.id)}
                    title={'Cut'}
                />
                <Button
                    className={styles.button.add}
                    styles={styles.button.margin}
                    event={() => onAddItem(item.id)}
                    title={'Add'}
                />
                <Button
                    className={styles.button.delete}
                    event={() => onDeleteItem(item.id)}
                    title={'x'}
                />
            </div>
        </div>
    )
}

export default Item
