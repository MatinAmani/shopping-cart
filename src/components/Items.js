import Item from './Item'

const Items = ({ items, styles, onDeleteItem, onAddItem, onCutItem }) => {
    return (
        <>
            {items.map((item) => {
                return (
                    <Item
                        key={item.id}
                        item={item}
                        styles={styles}
                        onDeleteItem={onDeleteItem}
                        onAddItem={onAddItem}
                        onCutItem={onCutItem}
                    />
                )
            })}
        </>
    )
}

export default Items
