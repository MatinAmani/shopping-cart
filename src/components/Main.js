import { useState } from 'react'
import Header from './Header'
import Items from './Items'
import AddItem from './AddItem'

const Main = () => {
    const [form, setForm] = useState(false)
    const [items, setItem] = useState([])
    var bill = 0

    const onNewItem = (item) => {
        let max = 0
        items.forEach((item) => (item.id > max ? (max = item.id) : max))
        max++

        item.id = max
        setItem([...items, item])
    }

    const onResetItem = () => {
        setItem(
            items.map((item) => ({
                ...item,
                quantity: 0,
                cost: item.price * item.quantity,
            }))
        )
    }

    const onDeleteItem = (id) => {
        setItem(items.filter((item) => item.id !== id))
    }

    const onAddItem = (id) => {
        setItem(
            // WTF?! using {} fuckes up with Main.js//37:17
            // When using () => {}, JS logic is required between {} and we need return value
            // For direct object manipulation, we use () => ({})
            items.map((item) =>
                item.id === id
                    ? {
                          ...item,
                          quantity: item.quantity + 1,
                          cost: item.cost + item.price,
                      }
                    : item
            )
        )
    }

    const onCutItem = (id) => {
        setItem(
            // WTF?! using {} fuckes up with Main.js//37:17
            // When using () => {}, JS logic is required between {} and we need return value
            // For direct object manipulation, we use () => ({})
            items.map((item) =>
                item.id === id
                    ? item.quantity > 0
                        ? {
                              ...item,
                              quantity: item.quantity - 1,
                              cost: item.cost - item.price,
                          }
                        : item
                    : item
            )
        )
    }

    const toggleForm = () => {
        setForm(!form)
    }

    items.forEach((item) => (bill += item.price * item.quantity))

    return (
        <div className={styles.main} style={{ width: '550px' }}>
            <Header
                styles={styles}
                onResetItem={onResetItem}
                onToggleForm={toggleForm}
                formStatus={form}
            />
            {form && <AddItem styles={styles} onNewItem={onNewItem} />}
            {items.length === 0 ? (
                <h4>No Items Exist.</h4>
            ) : (
                <Items
                    items={items}
                    styles={styles}
                    onDeleteItem={onDeleteItem}
                    onAddItem={onAddItem}
                    onCutItem={onCutItem}
                />
            )}
            <hr style={{ backgroundColor: '#aaa' }} />
            <h3>Total Price: ${bill}</h3>
        </div>
    )
}

const styles = {
    main: 'w3-panel w3-light-gray w3-border w3-border-black w3-display-topmiddle w3-round-large',
    button: {
        newItem: 'w3-btn w3-green w3-ripple',
        clear: 'w3-btn w3-red w3-margin w3-ripple',
        add: 'w3-button w3-blue w3-hover-blue w3-ripple',
        cut: 'w3-button w3-red w3-hover-red w3-ripple',
        disable: 'w3-button w3-gray w3-hover-gray',
        delete: 'w3-circle w3-red w3-small',
        block: 'w3-block w3-xlarge w3-text-black w3-green w3-ripple',
        margin: {
            marginLeft: '5px',
            marginRight: '5px',
        },
    },
    margin: {
        top: { marginTop: '10px' },
        right: { marginRight: '5px' },
        bottom: { marginBottom: '10px' },
        left: { marginLeft: '5px' },
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        h: { margin: '10px' },
    },
    item: {
        w3: 'w3-leftbar w3-border-blue w3-panel w3-white w3-hover-shadow',
        flex: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
    },
    form: {
        container: 'w3-container',
        input: 'w3-input w3-border',
        label: 'w3-large',
    },
}

export default Main
