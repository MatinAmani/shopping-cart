import Button from './Button'

const Header = ({ styles, onResetItem, onToggleForm, formStatus }) => {
    return (
        <div style={styles.header}>
            <h2 style={styles.header.h}>My Cart</h2>
            <div>
                <Button
                    className={
                        formStatus
                            ? styles.button.disable
                            : styles.button.newItem
                    }
                    event={onToggleForm}
                    title={formStatus ? 'Close' : 'New Item'}
                />
                <Button
                    className={styles.button.clear}
                    event={onResetItem}
                    title={'Reset Items'}
                />
            </div>
        </div>
    )
}

export default Header
